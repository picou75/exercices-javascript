function findLongestWordLength(s) {
    return s.split(' ')
      .reduce(function(x, y) {
        return Math.max(x, y.length)
      }, 0);
}

function findLongestWordLength(str) {
    let longerWords = 0;
    str.split(' ').forEach(word => {
      if (word.length > longerWords) {
        longerWords = word.length;
      }
    });
    return longerWords;
}

function findLongestWordLength(str) {
    return Math.max(...str.split(" ").map(word => word.length));
}