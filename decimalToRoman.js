function parseToRoman(number) {
    const decimal = [1000, 900, 500, 400, 100,90, 50, 40, 10, 9, 5, 4, 1];
    const roman = ["M","CM" ,"D","CD" ,"C","XC" , "L", "XL" , "X", "IX", "V", "IV", "I"];
  
    let res = "";
    for (let i = 0; i < decimal.length; i++) {
      while (number%decimal[i] < number) {
        res += roman[i];
        number -= decimal[i];
      }
    }
    return res;
  
  }
  
  console.log(parseToRoman(4)); // === "IV"
  console.log(parseToRoman(37)); // === "XXXVII"
  console.log(parseToRoman(143)); // === "CXLIII"
  console.log(parseToRoman(1234)); // === "MCCXXXIV"
  