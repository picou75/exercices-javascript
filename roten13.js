function toRot13(stringToTransform) {
    return stringToTransform.split('').map(char => {
      let code = char.charCodeAt(char);
      //pas ascii
      if (code < 65 || code > 92) {
        return String.fromCharCode(code);
      }
      else if (code < 78) {
        return String.fromCharCode(code + 13);
      } else {
        return String.fromCharCode(code - 13);
      }
    }).join('');
  }
  
  console.log(toRot13('URYYB JBEYQ')); // HELLO WORLD
  console.log(toRot13('BCRAPYNFFEBBZF')); // OPENCLASSROOMS
  console.log(toRot13('PRPV RFG ZBA PBQR FRPERG')); // CECI EST MON CODE SECRET