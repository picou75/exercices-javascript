function repeatStringNumTimes(str, num) {
    return num > 0 ? str + repeatStringNumTimes(str, num - 1) : '';
}

function repeatStringNumTimes(str, num) {
    let res = ""
    for (let i = 0; i < num; i++) {
      res += str;
    }
    return res;
}
  