function largestOfFour(arr) {
    let res = [];
    arr.forEach(array => {
      res.push(Math.max(...array));
    });
    return res;
}

function largestOfFour(arr) {
    return arr.map(function(group) {
      return group.reduce(function(prev, current) {
        return current > prev ? current : prev;
      });
    });
}

// RECURSIVE
function largestOfFour(arr, finalArr = []) {
    return !arr.length
      ? finalArr
      : largestOfFour(arr.slice(1), finalArr.concat(Math.max(...arr[0])))
}