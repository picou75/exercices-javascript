function factorialize(num) {
    let res = 1;
    while (num > 0) {
      res *= num;
      num--;
    }
    return res;
}

function factorialize(num) {
    if (num === 0) {
      return 1;
    }
    return num * factorialize(num - 1);
}
  

factorialize(5);  