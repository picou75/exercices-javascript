function confirmEnding(str, target) {
    return target === str.substring(str.length - target.length);
}
  
function confirmEnding(str, target) {
    return str.slice(str.length - target.length) === target;
}

function confirmEnding(str, target) {
    let reg = new RegExp(target + "$", "i");
    return reg.test(str);
}
  

confirmEnding("Bastian", "n");
confirmEnding("He has to give me a new name", "name");  